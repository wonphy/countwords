package com.fw.cc.dgitsystems.countwords;


import com.fw.cc.dgitsystems.countwords.service.StringProcessor;
import com.fw.cc.dgitsystems.countwords.service.WordCounter;
import com.fw.cc.dgitsystems.countwords.serviceimpl.CountWordsProcessor;
import com.fw.cc.dgitsystems.countwords.serviceimpl.WordLongerThanCounter;
import com.fw.cc.dgitsystems.countwords.serviceimpl.WordStartWithCounter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * A demo swing application.
 *
 * Created by wonph on 19/04/2016.
 */
public final class CountWordsDemo extends JFrame {

    private StringProcessor stringProcessor;
    private WordCounter[] wordCounterArray;

    private JComboBox wordCounterComboBox;
    private JTextArea wordCounterResultTextArea;
    private JTextArea stringProcessorInputTextArea;
    private JButton startStringProcessorButton;

    public CountWordsDemo() {
        super("Count Words Demo");

        stringProcessor = new CountWordsProcessor();
        wordCounterArray = new WordCounter[2];
        wordCounterArray[0] = new WordStartWithCounter('m', true);
        wordCounterArray[1] = new WordLongerThanCounter(5);
        stringProcessor.addWordCounter(wordCounterArray);

        setup();
    }

    public void setup() {
        JPanel top = new JPanel(new FlowLayout(FlowLayout.LEFT));
        top.add(new JLabel("Word Counters: "));
        wordCounterComboBox = new JComboBox();
        wordCounterComboBox.addItem("SELECT A WORD COUNTER TO VIEW RESULT");
        for (WordCounter wordCounter : wordCounterArray) {
            wordCounterComboBox.addItem(wordCounter);
        }
        wordCounterComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                wordCounterResultTextArea.setText("");
                if (null != wordCounterComboBox.getSelectedItem() && wordCounterComboBox.getSelectedItem() instanceof WordCounter) {
                    WordCounter wordCounter = (WordCounter) wordCounterComboBox.getSelectedItem();
                    wordCounterResultTextArea.setText(wordCounter.getResultToString());
                }
            }
        });
        top.add(wordCounterComboBox);
        add(top, BorderLayout.NORTH);

        JPanel middle = new JPanel(new BorderLayout());
        wordCounterResultTextArea = new JTextArea();
        wordCounterResultTextArea.setEditable(false);
        wordCounterResultTextArea.setBackground(Color.lightGray);
        JScrollPane outputScrollPane = new JScrollPane(wordCounterResultTextArea);
        outputScrollPane.setBorder(BorderFactory.createTitledBorder("Word Counting Output Area"));
        middle.add(outputScrollPane, BorderLayout.CENTER);
        stringProcessorInputTextArea = new JTextArea();
        stringProcessorInputTextArea.setRows(25);
        JScrollPane inputScrollPane = new JScrollPane(stringProcessorInputTextArea);
        inputScrollPane.setBorder(BorderFactory.createTitledBorder("String Input Area"));
        middle.add(inputScrollPane, BorderLayout.SOUTH);
        add(middle, BorderLayout.CENTER);

        startStringProcessorButton = new JButton("Process");
        startStringProcessorButton.addActionListener((ActionEvent e) -> {
            stringProcessor.process(stringProcessorInputTextArea.getText().trim());
            wordCounterResultTextArea.setText("String processing finished.");
            wordCounterComboBox.setSelectedIndex(0);
        });
        add(startStringProcessorButton, BorderLayout.SOUTH);

        pack();
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(new Dimension(1024, 768));
        this.setLocationByPlatform(true);
    }

    public void showUp() {
        setVisible(true);
        stringProcessorInputTextArea.requestFocus();
    }

    public static void main(String args[]) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new CountWordsDemo().showUp();
            }
        });
    }
}
