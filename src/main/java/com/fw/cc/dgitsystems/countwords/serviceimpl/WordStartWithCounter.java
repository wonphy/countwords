package com.fw.cc.dgitsystems.countwords.serviceimpl;


import com.fw.cc.dgitsystems.countwords.service.WordCounter;

/**
 * Word count that can count a word if it starts with a specific char,
 * it can ignore case if you specific it.
 *
 * Created by wonph on 19/04/2016.
 */
public class WordStartWithCounter implements WordCounter<Integer> {

    protected String _name;
    protected char _character;
    protected boolean _ignoreCase;

    protected int wordAmount;

    /**
     * Constructor method with character and whether ignore case specified.
     *
     * @param character
     * @param ignoreCase
     */
    public WordStartWithCounter(char character, boolean ignoreCase) {
        _character = character;
        _ignoreCase = ignoreCase;
        wordAmount = 0;
        _name = "Word Start With \"" + _character + "\"";
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void count(String word) {
        if (null != word) {
            final String theWord = word.trim();
            if (!theWord.isEmpty()) {
                char startChar = theWord.charAt(0);
                if (_character == startChar || (_ignoreCase && Character.toLowerCase(startChar) == Character.toLowerCase(_character))) {
                    wordAmount += 1;
                }
            }
        }
    }

    @Override
    public Integer getResult() {
        return wordAmount;
    }

    @Override
    public String getResultToString() {
        return String.format("There are %d words start with \"%c\".", wordAmount, _character);
    }

    @Override
    public void reset() {
        wordAmount = 0;
    }

    @Override
    public String toString() {
        return getName();
    }
}
