package com.fw.cc.dgitsystems.countwords.serviceimpl;

import com.fw.cc.dgitsystems.countwords.service.WordCounter;

import java.util.HashSet;
import java.util.Set;

/**
 * Word count which can count a word if it is longer than a specific length.
 *
 * Created by wonph on 19/04/2016.
 */
public class WordLongerThanCounter implements WordCounter<Set<String>> {

    protected String _name;
    protected int _longerThan;
    protected final Set<String> wordSet;

    /**
     * Constructor method with longer than number specified.
     *
     * @param longerThan
     */
    public WordLongerThanCounter(int longerThan) {
        _longerThan = longerThan;
        wordSet = new HashSet<>();
        _name = "Word Longer Than " + _longerThan;
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void count(String word) {
        if (null != word) {
            final String theWord = word.trim();
            if(theWord.length() > _longerThan) {
                wordSet.add(theWord);
            }
        }
    }

    @Override
    public Set<String> getResult() {
        return wordSet;
    }

    @Override
    public String getResultToString() {
        StringBuilder resultStringBuilder = new StringBuilder("Words longer than ");
        resultStringBuilder.append(_longerThan).append(": \n");
        wordSet.forEach(w -> resultStringBuilder.append("\t").append(w).append("\n"));
        return resultStringBuilder.toString();
    }

    @Override
    public void reset() {
        wordSet.clear();
    }

    @Override
    public String toString() {
        return getName();
    }
}
