package com.fw.cc.digitsystems.countwords;

import com.fw.cc.dgitsystems.countwords.serviceimpl.CountWordsProcessor;
import com.fw.cc.dgitsystems.countwords.serviceimpl.WordStartWithCounter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wonph on 20/04/2016.
 */
public class WordStartWithCounterTest {
    @Test
    public void testCount() {
        CountWordsProcessor countWordsProcessor = new CountWordsProcessor();
        WordStartWithCounter wordStartWithCounter1 = new WordStartWithCounter('m', true);
        countWordsProcessor.addWordCounter(wordStartWithCounter1);
        WordStartWithCounter wordStartWithCounter2 = new WordStartWithCounter('m', false);
        countWordsProcessor.addWordCounter(wordStartWithCounter2);
        countWordsProcessor.process("my Mum is a great woman.");
        Assert.assertEquals("There are 2 words start with \"m\" ignoring case.", 2, wordStartWithCounter1.getResult().intValue());
        Assert.assertEquals("There are 1 words start with \"m\" ignoring case.", 1, wordStartWithCounter2.getResult().intValue());
    }

    @Test
    public void testReset() {
        CountWordsProcessor countWordsProcessor = new CountWordsProcessor();
        WordStartWithCounter wordStartWithCounter = new WordStartWithCounter('m', true);
        countWordsProcessor.addWordCounter(wordStartWithCounter);
        countWordsProcessor.process("my Mum is a great woman.");
        Assert.assertEquals("There are 2 words start with \"m\" ignoring case.", 2, wordStartWithCounter.getResult().intValue());
        wordStartWithCounter.reset();
        Assert.assertEquals("There are 0 words start with \"m\" ignoring case.", 0, wordStartWithCounter.getResult().intValue());
    }
}
