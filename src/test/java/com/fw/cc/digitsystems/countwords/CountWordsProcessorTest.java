package com.fw.cc.digitsystems.countwords;

import com.fw.cc.dgitsystems.countwords.serviceimpl.CountWordsProcessor;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wonph on 20/04/2016.
 */
public class CountWordsProcessorTest {
    @Test
    public void testStringToWordArray() {
        CountWordsProcessor countWordsProcessor = new CountWordsProcessor();
        String string = "I love to know hi-tech trends.";
        String[] wordArray = countWordsProcessor.String2WordArray(string);
        Assert.assertEquals("There are 6 words in the word array.", 6, wordArray.length);
        Assert.assertEquals("There 5th word is \"hi-tech\".", "hi-tech", wordArray[4]);
    }
}
