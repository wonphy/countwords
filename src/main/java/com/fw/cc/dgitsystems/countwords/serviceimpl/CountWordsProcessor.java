package com.fw.cc.dgitsystems.countwords.serviceimpl;


import com.fw.cc.dgitsystems.countwords.service.StringProcessor;
import com.fw.cc.dgitsystems.countwords.service.WordCounter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * A typic string processor implementation.
 *
 * Created by wonph on 19/04/2016.
 */
public class CountWordsProcessor implements StringProcessor {

    protected final Set<WordCounter> wordCounterSet;

    public CountWordsProcessor() {
        wordCounterSet = new HashSet<>();
    }

    @Override
    public void addWordCounter(WordCounter... wordCounters) {
        for(WordCounter wordCounter : wordCounters) {
            if (null != wordCounter) {
                wordCounterSet.add(wordCounter);
            }
        }
    }

    @Override
    public void removeWordCounter(WordCounter... wordCounters) {
        for(WordCounter wordCounter : wordCounters) {
            if (null != wordCounter) {
                wordCounterSet.remove(wordCounter);
            }
        }
    }

    @Override
    public Set<WordCounter> getAllWordCounters() {
        Set<WordCounter> result = new HashSet<>();
        wordCounterSet.stream().forEach(wc->result.add(wc));
        return result;
    }

    // Prepare to process words, resetting all word counters.
    protected void prepare() {
        wordCounterSet.forEach(wc -> wc.reset());
    }

    public String[] String2WordArray(String string) {
        if (null == string || string.trim().isEmpty()) {
            return new String[0];
        }
        final String theString = string.trim();
        return theString.split("[^a-zA-Z\\-]");
    }

    @Override
    public void process(String string) {
        String[] wordArray = String2WordArray(string);
        prepare();
        Arrays.stream(wordArray).forEach(w -> {
            wordCounterSet.stream().forEach(wc -> wc.count(w));
        });
    }
}
