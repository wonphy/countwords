package com.fw.cc.digitsystems.countwords;

import com.fw.cc.dgitsystems.countwords.serviceimpl.CountWordsProcessor;
import com.fw.cc.dgitsystems.countwords.serviceimpl.WordLongerThanCounter;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by wonph on 20/04/2016.
 */
public class WordLongerThanCounterTest {
    @Test
    public void testCount() {
        CountWordsProcessor countWordsProcessor = new CountWordsProcessor();
        WordLongerThanCounter wordLongerThanCounter = new WordLongerThanCounter(5);
        countWordsProcessor.addWordCounter(wordLongerThanCounter);
        countWordsProcessor.process("I like to know hi-tech trends.");
        Assert.assertEquals("There are 2 words that are longer than 5.", 2, wordLongerThanCounter.getResult().size());
    }
}
