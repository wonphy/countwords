package com.fw.cc.dgitsystems.countwords.service;

/**
 * Word counter definition.
 *
 * Created by wonph on 19/04/2016.
 */
public interface WordCounter<R> {

    /**
     * Get name of the word counter.
     *
     * @return
     */
    String getName();

    /**
     * Count a word.
     *
     * @param word
     */
    void count(String word);

    /**
     * Get result.
     *
     * @return
     */
    R getResult();

    /**
     * Get a direct printable result int string.
     *
     * @return
     */
    String getResultToString();

    void reset();
}
