package com.fw.cc.dgitsystems.countwords.service;

import java.util.Set;

/**
 * String processor definition, it should maintains a list of word counter,
 * it can split a string into a word array and call word counters to process them one by one.
 *
 * Created by wonph on 19/04/2016.
 */
public interface StringProcessor {

    /**
     * Add a new word counter.
     *
     * @param wordCounters
     */
    void addWordCounter(WordCounter... wordCounters);

    /**
     * Remove a word counter.
     *
     * @param wordCounters
     */
    void removeWordCounter(WordCounter... wordCounters);

    /**
     * Get all added word counters.
     *
     * @return
     */
    Set<WordCounter> getAllWordCounters();

    /**
     * Process a string.
     *
     * @param string
     */
    void process(String string);
}
